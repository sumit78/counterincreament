import React from 'react';
import { connect } from 'react-redux';

import CounterControl from '../Component/Countercontrol';
import CounterOutput from '../Component/CounterOutput';


class Counter extends React.Component{
  state ={
    counter:0
  }

  counterChangeHandler = (action) => {
    switch(action){
      case 'inc':
        this.setState((prevState) => { return { counter: prevState.counter +1 }});
        break;
      case 'dec':
        this.setState((prevState) => { return {  counter:prevState.counter -1 }});
        break;
        default:
    }
  }
  render(){
    return(
      <div>
        <CounterOutput value={this.props.ctr} />
        <CounterControl label="Increment" clicked={this.props.onIncrementCounter} />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return{
    ctr:state.counter
  };
};

const mapDispatchToProps = dispatch => {
  return{
    onIncrementCounter : () => dispatch({type:'INCREMENT'})
  };
};

export default connect(mapStateToProps,mapDispatchToProps)(Counter);